#ifndef IODEVICE_H
#define IODEVICE_H

#include <QObject>

class IODevice : public QObject
{
  Q_OBJECT
public:
  explicit IODevice(QObject *parent = nullptr);

  virtual bool isOpen() = 0;

  virtual QString errorString() = 0;

signals:
  void openned();
  void rx_buf_ready(QByteArray buf);
  void clossed();

public slots:
  virtual bool open() = 0;
  virtual void close() = 0;
  virtual qint64 send(const char * data, qint64 size) = 0;
};

#endif // IODEVICE_H
