#include "InterfaceControlWidget.h"
#include "ui_InterfaceControlWidget.h"

#include <QSerialPortInfo>

#include <QDebug>

InterfaceControlWidget::InterfaceControlWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::InterfaceControlWidget)
{
  ui->setupUi(this);
  ui_init_InerfaceList();
  ui_refresh_comPortsList();
  ui_refresh_comBaudRatesList();
  ui_setConnectionState(false);

  connect(ui->com_refreshPortsList, SIGNAL(clicked()), this, SLOT(ui_refresh_comPortsList()));
}

InterfaceControlWidget::~InterfaceControlWidget()
{
  disconnect(ui->com_refreshPortsList, SIGNAL(clicked()), this, SLOT(ui_refresh_comPortsList()));
  delete ui;
}

void InterfaceControlWidget::setInterface(QString val)
{
  Interface interface;
  val = val.toLower();
  if (val.contains("tcp"))
    interface = TCP;
  else if (val.contains("com"))
    interface = COM;
  else
   interface = UnknownInterface;
  setInterface(interface);
}

void InterfaceControlWidget::setInterface(InterfaceControlWidget::Interface val)
{
  int idx = ui->Interface->findData((int)val);
  if (idx == -1)
  {
    qDebug() << metaObject()->className() << ": Set Interface failure";
    return;
  }
  if (idx == ui->Interface->currentIndex())
    on_Interface_currentIndexChanged(idx);
  else
    ui->Interface->setCurrentIndex(idx);
}

void InterfaceControlWidget::setComPort(QString name)
{
  int idx = ui->com_Port->findText(name);
  ui_refresh_comPortIdx(idx);
}

void InterfaceControlWidget::setComBaudRate(qint32 rate)
{
  setComBaudRate(QString::number(rate));
}

void InterfaceControlWidget::setComBaudRate(QString rate)
{
  int idx = ui->com_BaudRate->findText(rate);
  if (idx == -1)
    ui->com_BaudRate->setCurrentText(rate);
  else
    ui_refresh_comBaudRateIdx(idx);
}

QString InterfaceControlWidget::comPort()
{
  return ui->com_Port->currentText();
}

qint32 InterfaceControlWidget::comBaudRate()
{
  bool ok;
  int rate = ui->com_BaudRate->currentText().toInt(&ok);
  if (!ok)
    qDebug() << metaObject()->className() << "COM::Port baudrate text to int covert failure";
  return rate;
}

void InterfaceControlWidget::setTcpHost(QString name)
{
  ui->tcp_Host->setText(name);
}

void InterfaceControlWidget::setTcpPort(QString port)
{
  ui->tcp_Port->setText(port);
}

QString InterfaceControlWidget::tcpHost()
{
  return ui->tcp_Host->text();
}

quint16 InterfaceControlWidget::tcpPort()
{
  return ui->tcp_Port->text().toInt();
}

void InterfaceControlWidget::setWaitingState(bool flag)
{
  if (flag)
  {
    ui->ConnectionControl->setText("Waiting...");
    ui->Params->setEnabled(false);
  }
  else
    ui_setConnectionState(false);
}

void InterfaceControlWidget::setConnectionState(bool flag)
{
  ui_setConnectionState(flag);
}

void InterfaceControlWidget::ui_setConnectionState(bool flag)
{
  QString controlText;
  if (flag)
    controlText = "Disconnect";
  else
    controlText = "Connect";
  ui->ConnectionControl->setText(controlText);
  ui->Params->setEnabled(!flag);
  ui->Interface->setEnabled(!flag);
}

void InterfaceControlWidget::ui_init_InerfaceList()
{
  ui->Interface->addItem("COM", (int)COM);
  ui->Interface->addItem("TCP", (int)TCP);
}

void InterfaceControlWidget::ui_refresh_comPortsList()
{
  QString current = ui->com_Port->currentText();

  ui->com_Port->clear();

  ui->com_Port->addItem("");
  QList<QSerialPortInfo> portsList = QSerialPortInfo::availablePorts();
  foreach (QSerialPortInfo port, portsList)
    ui->com_Port->addItem(port.portName());
  setComPort(current);
}

void InterfaceControlWidget::ui_refresh_comBaudRatesList()
{
  QString current = ui->com_BaudRate->currentText();

  ui->com_BaudRate->clear();

  QList<qint32> ratesList = QSerialPortInfo::standardBaudRates();
  foreach (qint32 rate, ratesList)
    ui->com_BaudRate->addItem(QString::number(rate));

  setComBaudRate(current);
}

void InterfaceControlWidget::ui_refresh_comPortIdx(int idx)
{
  if (idx == -1)
  {
    qDebug() << metaObject()->className() << "Set COM::Port failure";
    return;
  }
  ui->com_Port->setCurrentIndex(idx);
}

void InterfaceControlWidget::ui_refresh_comBaudRateIdx(int idx)
{
  if (idx == -1)
  {
    qDebug() << metaObject()->className() << "Set COM::BaudRate failure";
    return;
  }
  ui->com_BaudRate->setCurrentIndex(idx);
}

void InterfaceControlWidget::on_Interface_currentIndexChanged(int index)
{
  Interface interface = (Interface)ui->Interface->itemData(index).toInt();
  switch(interface)
  {
  case COM:
    ui->Params->setCurrentWidget(ui->page_comParams);
    break;
  case TCP:
    ui->Params->setCurrentWidget(ui->page_tcpParams);
    break;
  }
  emit interfaceChanged(interface);
}

void InterfaceControlWidget::on_ConnectionControl_clicked()
{
  emit connectionControlClicked();
}
